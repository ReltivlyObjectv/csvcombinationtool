package tech.relativelyobjective.csvcombinationtool;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author ReltivlyObjectv
 * Contact: me@relativelyobjective.tech
 * 
 */

public class PrimaryWindow extends JFrame {
    private final MenuBar menu;
    private final JTextArea textArea;
    
    public PrimaryWindow() {
        super("CSV Combination Tool by Christian Russell");
        super.setLayout(new BorderLayout());
        menu = new MenuBar(this);
        super.add(menu);
        textArea = new JTextArea();
        JScrollPane scroller = new JScrollPane(textArea);
        super.add(scroller, BorderLayout.CENTER);
        super.setJMenuBar(menu);
        super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        super.setPreferredSize(new Dimension(850, 580));
        super.setSize(super.getPreferredSize());
    }
    public void resetWindow() {
        System.out.println("New Document");
        textArea.setText("");
    }
    public void openFolder() {
        System.out.println("Open Folder");
        textArea.setText(FileLoading.promptCombineFolderCSV(this, textArea.getText()));
    }
    public void save() {
        System.out.println("Save CSV");
        FileSaving.promptSaveToFile(this, textArea.getText());
    }
    public String getCurrentContents() {
        return textArea.getText();
    }
    public void setCurrentContents(String contents) {
        textArea.setText(contents);
    }
}
