package tech.relativelyobjective.csvcombinationtool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author ReltivlyObjectv
 * Contact: me@relativelyobjective.tech
 * 
 */
public class CSVFile {
    public class Row {
        //Convention: Header Label, Data
        public HashMap<String, String> data;
        public Row() {
            data = new HashMap<>();
        }
    }
    //Header Convention: Index, Label
    private HashMap<Integer, String> columnHeaders;
    private LinkedList<Row> documentContents = new LinkedList<>();
    
    private static HashMap<Integer, String> parseDocumentKeys(String rowContents) {
        System.out.println();
        System.out.println("HEADER: "+rowContents);
        HashMap<Integer, String> headers = new HashMap<>();
        String[] splitContents = rowContents.split(",");
        for (int i = 0; i < splitContents.length; i++) {
            headers.put(i, splitContents[i]);
        }
        for (Integer i : headers.keySet()) {
            System.out.printf("%d: %s\n", i, headers.get(i));
        }
        return headers;
    }
    
    private static String getFileContents(File file) throws FileNotFoundException, IOException {
        String contents = "";
        FileInputStream fis = new FileInputStream(file);
        int r=0;  
        while((r = fis.read()) != -1) {  
            contents += (char)r;
        }
        return contents;
    }
    
    public CSVFile(File location) throws IOException {
        String contents = getFileContents(location);
        initializeWithText(contents);
    }
    public CSVFile(String contents) {
        initializeWithText(contents);
    }
    private void initializeWithText(String contents) {
        System.out.printf("File Contents:\n%s\n",contents);
        String[] splitContents = contents.split("\n");
        for (int i = 0; i < splitContents.length; i++) {
            if (i == 0) {
                columnHeaders = parseDocumentKeys(splitContents[0]);
            } else {
                if (splitContents[i].replace(",", "").equals("")) {
                    continue;
                }
                Row newRow = new Row();
                String[] rowContentsSplit = splitContents[i].split(",");
                boolean hasContents = false;
                for (int k = 0; k < rowContentsSplit.length; k++) {
                    if (!rowContentsSplit[k].equals("")) {
                        hasContents = true;
                    }
                    newRow.data.put(columnHeaders.get(k), rowContentsSplit[k]);
                }
                if (hasContents) {
                    documentContents.addLast(newRow);
                }
            }
        }
    }
    public Row[] getRows() {
        return documentContents.toArray(new Row[documentContents.size()]);
    }
    public void addRow(Row r) {
        Row newLocalRow = new Row();
        
        for (String key : r.data.keySet()) {
            int currentKeyIndex = -1;
            String newKey = null;
            for (int i = 0; i < columnHeaders.keySet().size(); i++) {
                if (columnHeaders.get(i).equalsIgnoreCase(key)) {
                    currentKeyIndex = i;
                    newKey = columnHeaders.get(i);
                }
            }
            if (currentKeyIndex < 0) {
                //Key not already defined
                columnHeaders.put(columnHeaders.size(), key);
            }
            newLocalRow.data.put(newKey, r.data.get(key));
        }
        documentContents.addLast(newLocalRow);
    }
    @Override
    public String toString() {
        String contents = "";
        for (int i = 0; i < columnHeaders.keySet().size(); i++) {
            if (i > 0) {
                contents+=",";
            }
            contents += columnHeaders.get(i);
        }
        contents += "\n";
        for (Row r : documentContents) {
            for (int i = 0; i < columnHeaders.keySet().size(); i++) {
                if (i > 0) {
                    contents+=",";
                }
                String textToAdd = r.data.get(columnHeaders.get(i));
                contents += textToAdd == null ? "" : textToAdd;
            }
            contents += "\n";
        }
        
        return contents;
    }
}
