package tech.relativelyobjective.csvcombinationtool;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

/**
 *
 * @author ReltivlyObjectv
 * Contact: me@relativelyobjective.tech
 * 
 */
public class MenuBar extends JMenuBar {
    public MenuBar(PrimaryWindow window) {
        //Create Menu
        JMenu fileMenu = new JMenu("File");
        
        //New
        JMenuItem newFile = new JMenuItem("New Merge");
        newFile.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_N, 
                Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        newFile.addActionListener((ActionEvent e) -> {
            window.resetWindow();
        });
        fileMenu.add(newFile);
        
        //Open
        JMenuItem openFiles = new JMenuItem("Open Folder...");
        openFiles.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_O, 
                Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        openFiles.addActionListener((ActionEvent e) -> {
            window.openFolder();
        });
        fileMenu.add(openFiles);
        
        //Save
        JMenuItem saveFile = new JMenuItem("Save CSV...");
        saveFile.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_S, 
                Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        saveFile.addActionListener((ActionEvent e) -> {
            window.save();
        });
        fileMenu.add(saveFile);
        
        //Add Menu
        super.add(fileMenu);
    }
}
