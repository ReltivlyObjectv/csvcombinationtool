package tech.relativelyobjective.csvcombinationtool;

import javax.swing.SwingUtilities;

/**
 *
 * @author ReltivlyObjectv
 * Contact: me@relativelyobjective.tech
 * 
 */

public class CSVCombinationTool {
    public static void main(String[] args) {
        //Create main frame
        SwingUtilities.invokeLater(() -> {
            PrimaryWindow primaryWindow = new PrimaryWindow();
            primaryWindow.setVisible(true);
        });
    }
}
