package tech.relativelyobjective.csvcombinationtool;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author ReltivlyObjectv
 * Contact: me@relativelyobjective.tech
 * 
 */

public class FileSaving {
    private static File workingDirectory = new File(".");
    
    public static void promptSaveToFile(JFrame window, String contents) {
        File saveLocation = promptForLocation(window);
        if (saveLocation == null) {
            return;
        }
        workingDirectory = saveLocation.getParentFile();
        saveContentsToLocation(contents, saveLocation);
    }
    
    private static File promptForLocation(JFrame window) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(workingDirectory);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Comma-Separated Values", "csv");
        fileChooser.setFileFilter(filter);
        if (fileChooser.showSaveDialog(window) == JFileChooser.APPROVE_OPTION) {
            File tempSaveLocation = fileChooser.getSelectedFile();
            if (!tempSaveLocation.toString().endsWith(".csv")) {
                tempSaveLocation = new File(tempSaveLocation.toString() + ".csv");
            }
            if (tempSaveLocation.exists()) {
                int response = JOptionPane.showConfirmDialog(null,
                    String.format("The file %s already exists. Do you want to replace it?", tempSaveLocation.getName()),
                    String.format("%s Exists", tempSaveLocation.getName()),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
                if (response != JOptionPane.YES_OPTION) {
                    return null;
                } 
            }
            return tempSaveLocation;
        }
        return null;
    }
    private static void saveContentsToLocation(String contents, File saveDestination) {
        try {
            PrintWriter out = new PrintWriter(saveDestination.getAbsoluteFile());
            out.print(contents);
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileSaving.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
