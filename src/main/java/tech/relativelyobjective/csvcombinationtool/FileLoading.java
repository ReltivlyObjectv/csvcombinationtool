package tech.relativelyobjective.csvcombinationtool;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import tech.relativelyobjective.csvcombinationtool.CSVFile.Row;

/**
 *
 * @author ReltivlyObjectv
 * Contact: me@relativelyobjective.tech
 * 
 */

public class FileLoading {
    private static File workingDirectory = new File(".");
    
    public static String promptCombineFolderCSV(PrimaryWindow window, String existingContents) {
        File loadLocation = promptForLocation(window);
        if (loadLocation == null) {
            return existingContents;
        }
        if (workingDirectory.isDirectory()) {
            workingDirectory = loadLocation;
        } else {
            workingDirectory = loadLocation.getParentFile();
        }
        if (!workingDirectory.exists()) {
            System.out.println(workingDirectory.getAbsolutePath()+" does not exist.");
            return existingContents;
        }
        File[] contents = getFolderCSVs(workingDirectory);
        System.out.printf("Found %d file(s)\n", contents.length);
        CSVFile newDocument;
        if (existingContents.equals("")) {
            newDocument = null;
        } else {
            newDocument = new CSVFile(existingContents);
        }
        for (int i = 0; i < contents.length; i++) {
            CSVFile documentToAdd;
            try {
                documentToAdd = new CSVFile(contents[i]);
                if (newDocument == null) {
                    newDocument = documentToAdd;
                    continue;
                }
            } catch (IOException ex) {
                System.out.println("Unable to open file: "+contents[i].getAbsolutePath());
                continue;
            }
            Row[] documentToAddRows = documentToAdd.getRows();
            for (int r = 0; r < documentToAddRows.length; r++) {
                newDocument.addRow(documentToAddRows[r]);
            }
        }
        System.out.println(newDocument.toString());
        return newDocument.toString();
    }
    private static File promptForLocation(JFrame window) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(workingDirectory);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (fileChooser.showOpenDialog(window) == JFileChooser.APPROVE_OPTION) {
            System.out.printf("Opening Folder: %s\n", fileChooser.getSelectedFile().getAbsolutePath());
            return fileChooser.getSelectedFile();
        }
        return null;
    }
    
    private static File[] getFolderCSVs(File parent) {
        LinkedList<File> folderCSVs = new LinkedList<>();
        for (String s : parent.list()) {
            File currentFile = new File(parent.getAbsolutePath() + File.separatorChar + s);
            if (currentFile.getAbsolutePath().toLowerCase().endsWith(".csv")) {
                System.out.println("Found CSV: "+currentFile.getAbsolutePath()+" (CSV)");
                folderCSVs.addFirst(currentFile);
            } else {
                //System.out.println(currentFile.getAbsolutePath()+" (NOT)");
            }
        }
        return folderCSVs.toArray(new File[folderCSVs.size()]);
    }
    
}
